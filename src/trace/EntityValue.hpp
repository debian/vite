/**
 *
 * @file src/trace/EntityValue.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef ENTITYVALUE_HPP
#define ENTITYVALUE_HPP
#include <QObject>

#include <map>
#include "common/common.hpp"
#include "trace/values/Values.hpp"

/*!
 * \file EntityValue.hpp
 */
class EntityType;

/*!
 * \class EntityValue
 * \brief Describe the value of an entity
 */
class EntityValue : public QObject
{
    Q_OBJECT

private:
    Name _name;
    EntityType *_type;
    std::map<std::string, Value *> _opt;

    Color _filecolor;
    Color _usedcolor;
    bool _visible;

public:
    /*!
     * \fn EntityValue(const Name &name, EntityType *type, std::map<std::string, Value *> opt);
     * \brief EntityValue Constructor
     * \param name Name of the value
     * \param type EntityType to which the value apply
     * \param opt Extra fields
     */
    EntityValue(const Name &name, EntityType *type, std::map<std::string, Value *> opt);
    ~EntityValue() override;

    /*!
     * \fn get_name() const
     * \brief Get the name of the value
     */
    Name get_Name() const;

    std::string get_name() const;
    std::string get_alias() const;

    /*!
     * \fn get_type() const
     * \brief Get the entity type of the value
     */
    const EntityType *get_type() const;

    /*!
     * \fn get_extra_fields() const
     * \brief Get the extra fields
     */
    const std::map<std::string, Value *> *get_extra_fields() const;

    const Color *get_used_color() const;
    const Color *get_file_color() const;
    bool get_visible() const;
    EntityClass_t get_class() const;

    void set_used_color(const Color &c);
    void set_visible(bool b);

    void reload_file_color();

Q_SIGNALS:
    void changedColor(EntityValue *);
    void changedVisible(EntityValue *);
};

#endif
