/**
 *
 * @file src/common/Info.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 *\file info.cpp
 */

#include "common/common.hpp"
#include "common/Info.hpp"
#include "common/Session.hpp"
#include <string>
using namespace std;

unsigned int Info::Screen::width = 800;
unsigned int Info::Screen::height = 600;

Element_pos Info::Container::x_min = 0;
Element_pos Info::Container::x_max = 0;
Element_pos Info::Container::y_min = 0;
Element_pos Info::Container::y_max = 0;

Element_pos Info::Entity::x_min = 0;
Element_pos Info::Entity::x_max = 0;
Element_pos Info::Entity::y_min = 0;
Element_pos Info::Entity::y_max = 0;

Element_pos Info::Render::width = 100; /* 100 OpenGL units for 1 pixel  */
Element_pos Info::Render::height = 100; /* 100 OpenGL units for 1 pixel  */

bool Info::Render::_key_alt = false;
bool Info::Render::_key_ctrl = false;
Element_pos Info::Render::_x_min_visible = 0.0;
Element_pos Info::Render::_x_max_visible = 0.0;
Element_pos Info::Render::_x_min = 0.0;
Element_pos Info::Render::_x_max = 0.0;

Element_pos Info::Render::_info_x = 0.0;
Element_pos Info::Render::_info_y = 0.0;
Element_pos Info::Render::_info_accurate = 0.0;
int Info::Render::_arrows_shape = 0;
bool Info::Render::_no_arrows = false;
bool Info::Render::_no_events = false;
bool Info::Render::_shaded_states = Session::getSession().get_shaded_states_setting();
// true;/* By default, enable shaded state */
bool Info::Render::_vertical_line = Session::getSession().get_vertical_line_setting();
// true;/* By default, enable vertical line */

bool Info::Splitter::split = false;
bool Info::Splitter::load_splitted = false;
bool Info::Splitter::preview = false;
std::string Info::Splitter::path;
std::string Info::Splitter::filename;
std::string Info::Splitter::xml_filename;
Element_pos Info::Splitter::_x_min = 0.0;
Element_pos Info::Splitter::_x_max = 0.0;

int Info::Trace::depth = 0;

void Info::release_all() {

    Info::Container::x_min = 0;
    Info::Container::x_max = 0;
    Info::Container::y_min = 0;
    Info::Container::y_max = 0;

    Info::Entity::x_min = 0;
    Info::Entity::x_max = 0;
    Info::Entity::y_min = 0;
    Info::Entity::y_max = 0;

    Info::Render::_x_min_visible = 0.0;
    Info::Render::_x_max_visible = 0.0;
}
