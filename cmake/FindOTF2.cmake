#
# Find the OTF2 libraries and include dir
#

# OTF2_INCLUDE_DIR  - Directories to include to use OTF2
# OTF2_LIBRARY    - Files to link against to use OTF2
# OTF2_FOUND        - When false, don't try to use OTF2
#
# OTF2_DIR can be used to make it simpler to find the various include
# directories and compiled libraries when OTF2 was not installed in the
# usual/well-known directories (e.g. because you made an in tree-source
# compilation or because you installed it in an "unusual" directory).
# Just set OTF2_DIR it to your specific installation directory
#
FIND_LIBRARY(OTF2_LIBRARY otf2
  PATHS
  /usr/lib
  /usr/local/lib
  ${OTF2_DIR}/lib
)

IF(OTF2_LIBRARY)
  GET_FILENAME_COMPONENT(OTF2_GUESSED_INCLUDE_DIR_tmp "${OTF2_LIBRARY}" PATH)
  STRING(REGEX REPLACE "lib$" "include" OTF2_GUESSED_INCLUDE_DIR "${OTF2_GUESSED_INCLUDE_DIR_tmp}")
ENDIF(OTF2_LIBRARY)

FIND_PATH( OTF2_INCLUDE_DIR otf2/otf2.h
  PATHS
  ${OTF2_GUESSED_INCLUDE_DIR}
  ${OTF2_DIR}/include
  /usr/include
  /usr/local/include
  ${CMAKE_SOURCE_DIR}/externals/otf2/include/
)

IF( OTF2_INCLUDE_DIR )
  IF( OTF2_LIBRARY )
    SET( OTF2_FOUND "YES" )
    MARK_AS_ADVANCED( OTF2_DIR )
    MARK_AS_ADVANCED( OTF2_INCLUDE_DIR )
    MARK_AS_ADVANCED( OTF2_LIBRARY )
  ENDIF( OTF2_LIBRARY )
ENDIF( OTF2_INCLUDE_DIR )

IF( NOT OTF2_FOUND )
  MESSAGE("OTF2 installation was not found. Please provide OTF2_DIR:")
  MESSAGE("  - through the GUI when working with ccmake, ")
  MESSAGE("  - as a command line argument when working with cmake e.g. ")
  MESSAGE("    cmake .. -DOTF2_DIR:PATH=/usr/local/otf ")
  MESSAGE("Note: the following message is triggered by cmake on the first ")
  MESSAGE("    undefined necessary PATH variable (e.g. OTF2_INCLUDE_DIR).")
  MESSAGE("    Providing OTF2_DIR (as above described) is probably the")
  MESSAGE("    simplest solution unless you have a really customized/odd")
  MESSAGE("    OTF2 installation...")
  SET(OTF2_DIR "" CACHE PATH "Root of OTF2 install tree." )
ENDIF( NOT OTF2_FOUND )
