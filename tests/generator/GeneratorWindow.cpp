/**
 *
 * @file tests/generator/GeneratorWindow.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "GeneratorWindow.hpp"
#include <iostream>

GeneratorWindow::GeneratorWindow (QWidget* parent, QApplication* app):QMainWindow (parent){
    _app = app;
    setupUi(this);

    _gene = NULL;

    _ui_name          = qFindChild<QLineEdit*>(this, "fileNameEdit")           ;
    if (_ui_name == NULL)
        fprintf (stderr, "Cannot find the name box in the .ui file")           ;

    _ui_depth         = qFindChild<QLineEdit*>(this, "depthEdit")              ;
    if (_ui_depth == NULL)
        fprintf (stderr, "Cannot find the depth box in the .ui file")          ;

    _ui_procNbr       = qFindChild<QLineEdit*>(this, "procNumberEdit")         ;
    if (_ui_procNbr == NULL)
        fprintf (stderr, "Cannot find the proc number box in the .ui file")    ;

    _ui_numberOfState = qFindChild<QLineEdit*>(this, "stateNumberEdit")        ;
    if (_ui_numberOfState == NULL)
        fprintf (stderr, "Cannot find the number of state box in the .ui file");

    _ui_linkNbr       = qFindChild<QLineEdit*>(this, "linkEdit")               ;
    if (_ui_linkNbr == NULL)
        fprintf (stderr, "Cannot find the link box in the .ui file")           ;

    _ui_counterNbr    = qFindChild<QLineEdit*>(this, "counterEdit")            ;
    if (_ui_counterNbr == NULL)
        fprintf (stderr, "Cannot find the counter box in the .ui file")        ;

    _ui_linkTypeNbr   = qFindChild<QLineEdit*>(this, "linkType")               ;
    if (_ui_linkTypeNbr == NULL)
        fprintf (stderr, "Cannot find the link type in the .ui file")          ;

    _ui_eventTypeNbr= qFindChild<QLineEdit*>(this, "eventType")                ;
    if (_ui_eventTypeNbr == NULL)
        fprintf (stderr, "Cannot find the event type in the .ui file")         ;

    _ui_eventNbr      = qFindChild<QLineEdit*>(this, "eventEdit")              ;
    if (_ui_eventNbr == NULL)
        fprintf (stderr, "Cannot find the event box in the .ui file")          ;

    _ui_event         = qFindChild<QCheckBox*>(this, "eventCheck")             ;
    if (_ui_event == NULL)
        fprintf (stderr, "Cannot find the event checkbox in the .ui file")     ;

    _ui_counter       = qFindChild<QCheckBox*>(this, "counterCheck")           ;
    if (_ui_counter == NULL)
        fprintf (stderr, "Cannot find the counter checkbox in the .ui file")   ;

    _ui_link          = qFindChild<QCheckBox*>(this, "linkCheck")              ;
    if (_ui_link == NULL)
        fprintf (stderr, "Cannot find the link checkbox in the .ui file")      ;

    _ui_eventLabel    = qFindChild<QLabel*>(this, "eventLabel")                ;
    if (_ui_event == NULL)
        fprintf (stderr, "Cannot find the event label in the .ui file")        ;

    _ui_counterLabel  = qFindChild<QLabel*>(this, "counterLabel")              ;
    if (_ui_counter == NULL)
        fprintf (stderr, "Cannot find the counter label in the .ui file")      ;

    _ui_linkLabel     = qFindChild<QLabel*>(this, "linkLabel")                 ;
    if (_ui_link == NULL)
        fprintf (stderr, "Cannot find the link label in the .ui file")         ;

    _ui_paje          = qFindChild<QCheckBox*>(this, "paje")                   ;
    if (_ui_paje == NULL)
        fprintf (stderr, "Cannot find the paje checkbox in the .ui file")      ;

    _ui_tau           = qFindChild<QCheckBox*>(this, "tau")                    ;
    if (_ui_counter == NULL)
        fprintf (stderr, "Cannot find the tau checkbox in the .ui file")       ;

    _ui_otf           = qFindChild<QCheckBox*>(this, "otf")                    ;
    if (_ui_link == NULL)
        fprintf (stderr, "Cannot find the otf checkbox in the .ui file")       ;

    generateButton = qFindChild<QPushButton*>(this, "generateButton");
    if (generateButton == NULL)
        fprintf (stderr, "Cannot find the generate button in the .ui file");

    resetButton = qFindChild<QPushButton*>(this, "resetButton");
    if (resetButton == NULL)
        fprintf (stderr, "Cannot find the reset button in the .ui file");

    _ui_numberOfLine = qFindChild<QLineEdit*>(this, "lineNumberEdit")          ;
    if ( _ui_numberOfLine == NULL)
        fprintf (stderr, "Cannot find the number of line in the .ui file");

    //    QMetaObject::connectSlotsByName (parent);
    QObject::connect (generateButton, SIGNAL (clicked ()), this, SLOT (generateButton_clicked ()));
    QObject::connect (resetButton, SIGNAL (clicked ()), this, SLOT (resetButton_clicked ()));


    this->show ();
}

GeneratorWindow::~GeneratorWindow (){
}

void GeneratorWindow::generate (){
    _gene->generate ();
}

void GeneratorWindow::setParam (){
    QString nb;

    _name          = (_ui_name->text () == "")?NAME:_ui_name->text ()                                    ;
    _depth         = (_ui_depth->text () == "")?DEPTH:_ui_depth->text ().toInt ()                        ;
    _procNbr       = (_ui_procNbr->text () == "")?PROCNBR:_ui_procNbr->text ().toInt ()                  ;
    _numberOfState = (_ui_numberOfState->text () == "")?NUMBEROFSTATE:_ui_numberOfState->text ().toInt ();
    _numberOfLine  = (_ui_numberOfLine->text () == "")?NUMBEROFLINE:_ui_numberOfLine->text ().toInt ()   ;

    _paje = _ui_paje->isChecked ();
    _otf  = _ui_otf->isChecked  ();
    _tau  = _ui_tau->isChecked  ();

    nb = _ui_counterNbr->text ();
    if (_ui_counter->isChecked () && (nb != ""))
        _counterNbr = (nb.toInt ()<_procNbr)?nb.toInt ():_procNbr;
    else
        _counterNbr = COUNTERNBR;



    nb = _ui_linkNbr->text ();
    if (_ui_link->isChecked () && (nb != "")){
        _linkNbr = (nb.toInt ()<_procNbr)?nb.toInt ():_procNbr;
        _linkTypeNbr = _ui_linkTypeNbr->text ().toInt ();
    }
    else{
        _linkNbr = LINKNBR;
        _linkTypeNbr = LINKTYPENBR;
    }
    nb = _ui_linkTypeNbr->text ();
    if (_ui_link->isChecked () && (nb != ""))
        _linkTypeNbr = nb.toInt ();
    else
        _linkTypeNbr = LINKTYPENBR;

    
    nb = _ui_eventNbr->text ();
    if (_ui_event->isChecked () && (nb != ""))
        _eventNbr = (nb.toInt ()<_procNbr)?nb.toInt ():_procNbr;
    else
        _eventNbr = EVENTNBR;
    nb = _ui_eventTypeNbr->text ();
    if (_ui_event->isChecked () && (nb != ""))
        _eventTypeNbr = nb.toInt ();
    else
        _eventTypeNbr = EVENTTYPENBR;
    
    std::cout << " Valeurs rentrees : " << std::endl << " nom : " << _name.toStdString ()  << std::endl << " depth " << _depth  << std::endl << " proc nbr " << _procNbr  << std::endl << "nb of state " << _numberOfState  << std::endl << "nbr line " << _numberOfLine  << std::endl << "count nbr " << _counterNbr  << std::endl << "event nbr " << _eventNbr  << std::endl << "link nbr " << _linkNbr  << std::endl << "event type nbr " << _eventTypeNbr  << std::endl << "link type nbr " << _linkTypeNbr  << std::endl;

    if (_gene)
        delete (_gene);
    _gene = new Generator (_name,
                           _depth, _procNbr, _numberOfState, _numberOfLine,
                           _counterNbr, _eventNbr, _linkNbr,
                           _eventTypeNbr, _linkTypeNbr,
                           _paje, _otf, _tau);

}

void GeneratorWindow::hideCounter (){
    _ui_counterLabel->hide ();
    _ui_counterNbr->hide  ();
}

void GeneratorWindow::hideLink (){
    _ui_linkLabel->hide ();
    _ui_linkNbr->hide  ();
}

void GeneratorWindow::hideEvent (){
    _ui_eventLabel->hide ();
    _ui_eventNbr->hide  ();
}

void GeneratorWindow::showCounter (){
    _ui_counterLabel->show ();
    _ui_counterNbr->show  ();
}

void GeneratorWindow::showLink (){
    _ui_linkLabel->show ();
    _ui_linkNbr->show  ();
}

void GeneratorWindow::showEvent (){
    _ui_eventLabel->show ();
    _ui_eventNbr->show  ();
}

void GeneratorWindow::finishGeneration (){
    cleanParam ()  ;
    if (_gene)
        delete _gene;
    _gene = NULL;
}

void GeneratorWindow::cleanParam (){
    // Class param
    _name          = NAME         ;
    _depth         = DEPTH        ;
    _procNbr       = PROCNBR      ;
    _numberOfLine  = NUMBEROFLINE ;
    _numberOfState = NUMBEROFSTATE;
    _linkNbr       = LINKNBR      ;
    _eventNbr      = EVENTNBR     ;
    _counterNbr    = COUNTERNBR   ;

    // UI edit param
    _ui_name->setText          ("");
    _ui_depth->setText         ("");
    _ui_procNbr->setText       ("");
    _ui_numberOfLine->setText  ("");
    _ui_numberOfState->setText ("");
    _ui_linkNbr->setText       ("");
    _ui_eventNbr->setText      ("");
    _ui_counterNbr->setText    ("");
    _ui_eventTypeNbr->setText    ("");

    // UI checkbox
    _ui_link->setChecked    (true);
    _ui_event->setChecked   (true);
    _ui_counter->setChecked (true);
    // Ensure fields are visible
    showEvent   ();
    showLink    ();
    showCounter ();
}


void GeneratorWindow::resetButton_clicked (){
    fprintf (stderr, "Reset ..... \n\n\n");
    finishGeneration ();
}

void GeneratorWindow::generateButton_clicked (){
    fprintf (stderr, "Generation ..... \n\n\n");
    setParam ();
    generate ();
    cleanParam ();
}

int GeneratorWindow::run (){
    return _app->exec ();
}
