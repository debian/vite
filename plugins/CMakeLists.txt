#
#
#

### ViTE optional plugins
option(VITE_PLUGINS_DISTRIBUTIONS
  "Enable the distribution plugin." OFF)
option(VITE_PLUGINS_TRACEINFOS
  "Enable the TraceInfos plugin." OFF)
option(VITE_PLUGINS_MATRIX_VISUALIZER
  "Enable the MatrixVisualizer plugin." OFF)
option(VITE_PLUGINS_CRITICAL_PATH
  "Enable the CriticalPath plugin." OFF)

include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_BINARY_DIR}/src/common)

if(VITE_PLUGINS_DISTRIBUTIONS)
  add_subdirectory(Distribution)
endif()

if(VITE_PLUGINS_TRACEINFOS)
  add_subdirectory(TraceInfos)
endif()

if(VITE_PLUGINS_MATRIX_VISUALIZER)
  add_subdirectory(MatrixVisualizer)
endif()

if(VITE_PLUGINS_CRITICAL_PATH)
  add_subdirectory(CriticalPath)
endif()
