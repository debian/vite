/**
 *
 * @file src/statistics/ChartView.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Luca Bourroux
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 *
 * @date 2024-07-17
 */
#pragma once

#include <QtWidgets/qscrollbar.h>
#include <QtCharts/qchartview.h>
#include <QtWidgets/qapplication.h>
#include <QObject>
#include "ChartCompatibility.hpp"

class Chart_View : public QChartView
{
    Q_OBJECT
private:
    qreal _factor = 1.0;
    QPoint _last_mouse_pos;

    bool _restrict_scroll_x = true;
    bool _restrict_scroll_y = false;
    bool _ignore_scrollbar = false;

    QScrollBar &_x_scroll;
    QScrollBar &_y_scroll;

    QPointF _scroll_value;

public:
    Chart_View(
        QScrollBar &x_scroll,
        QScrollBar &y_scroll,
        QChart *chart,
        QWidget *parent = nullptr);

    void restrict_scroll(bool x, bool y);

    void reset_scroll_zoom();

Q_SIGNALS:
    void chart_focused();

protected:
    virtual void focusInEvent(QFocusEvent *event) override;

    void wheelEvent(QWheelEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
public Q_SLOTS:
    void on_x_scroll_changed(int x);
    void on_y_scroll_changed(int x);
};
