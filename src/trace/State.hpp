/**
 *
 * @file src/trace/State.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef STATE_HPP
#define STATE_HPP

/*!
 * \file State.hpp
 */

#include "trace/Entitys.hpp"

class State;
class StateType;

/*!
 * \class State
 * \brief Describe the state of a container
 */
class State : public Entity
{
private:
    Date _start, _end;
    StateType *_type;
    EntityValue *_value;

public:
    /*!
     * \brief Constructor
     */
    State(Date start, Date end, StateType *type, Container *container, EntityValue *value, std::map<std::string, Value *> opt);
    State();
    /*!
     * \fn get_start_time() const;
     * \brief Get the start time of the state
     */
    Date get_start_time() const;

    /*!
     * \fn get_end_time() const
     * \brief Get the end time of the state
     */
    Date get_end_time() const;

    /*!
     * \fn get_duration() const
     * \brief Get the duration of the state
     */
    double get_duration() const;

    /*!
     * \fn get_type() const
     * \brief Get the type of the state
     */
    const StateType *get_type() const;

    /*!
     * \fn get_value() const
     * \brief Get the value of the state
     * \return Pointer to the Entityvalue or NULL if it has no value
     */
    // const EntityValue *get_value() const;

    EntityValue *get_value() const;

    /*!
     * \fn set_left_date(Date)
     * \brief To set the left date
     */
    void set_left_date(Date);
};

#endif
