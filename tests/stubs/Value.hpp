/**
 *
 * @file tests/stubs/Value.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef VALUE_HPP
#define VALUE_HPP

/*!
 *
 * \file Value.hpp
 * \author Clément Vuchener
 * \brief Contains the definition of the class Value
 * \date 2009 January 30th
 *
 */


#include <string>

// For std::setprecision
#include <iomanip>
#include<iostream>

// For dots or commas separator in double numbers
#include <locale.h>


/*!
 *
 * \class Value
 * \brief Abstract class to store calue used in the trace
 *
 */
class Value {
public:

    virtual ~Value(){};

    /*!
     *
     * \fn to_string() const = 0
     * \brief a to string method.
     * \return the value in a string format.
     *
     */
    virtual std::string to_string() const = 0;

    /*!
     * \brief The precision for printing double with std::cout or printf.
     */
    static const int _PRECISION = 15;

};

#endif // VALUE_HPP
