/**
 *
 * @file tests/generator/TauGenerator.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "TauGenerator.hpp"


TauGenerator::TauGenerator   (){
}

TauGenerator::~TauGenerator  (){
}

void TauGenerator::initTrace (QString name, int depth, int procNbr, int stateNbr, int eventNbr, int linkTypeNbr, int varNbr){
}

void TauGenerator::addState  (int proc, int state, double time){
}

void TauGenerator::startLink (int proc, int type , double time){
}

void TauGenerator::endLink   (int proc, int type , double time){
}

void TauGenerator::addEvent  (int proc, int type , double time){
}

void TauGenerator::incCpt    (int proc, int var  , double time){
}

void TauGenerator::decCpt    (int proc, int var  , double time){
}

void TauGenerator::endTrace  (){
}
