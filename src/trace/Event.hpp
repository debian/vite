/**
 *
 * @file src/trace/Event.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef EVENT_HPP
#define EVENT_HPP

/*!
 * \file Event.hpp
 */

#include "trace/Entity.hpp"

class Container;
class EntityValue;
class Event;
class EventType;
template <typename E>
class BinaryTree;

/*!
 * \class Event
 * \brief Describe an event
 */
class Event : public Entity
{
public:
    typedef std::list<Event *> Vector;
    typedef std::list<Event *>::const_iterator VectorIt;
    typedef BinaryTree<Event> Tree;

private:
    Date _time;
    EventType *_type;
    EntityValue *_value;

public:
    /*!
     * \fn Event(Date time, EventType *type, Container *container, EntityValue *value, std::map<std::string, Value *> opt)
     * \brief Event Constructor
     * \param time Date of the event
     * \param type Type of the event
     * \param container Container of the event
     * \param value Value of the event
     * \param opt : optional data in the event
     */
    Event(Date time, EventType *type, Container *container, EntityValue *value, std::map<std::string, Value *> opt);

    /*!
     * \fn get_time() const
     * \brief Get the time of the event
     */
    Date get_time() const;

    /*!
     * \fn get_type() const
     * \brief Get the type of the event
     */
    const EventType *get_type() const;

    /*!
     * \fn get_value() const
     * \brief Get the value of the event
     * \return Pointer to the Entityvalue or NULL if it has no value
     */
    EntityValue *get_value() const;
};

#endif
