/**
 *
 * @file plugins/MatrixVisualizer/Parsers/OrderParser.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "OrderParser.hpp"
#include "Readers/Pastix.hpp"

OrderParser::OrderParser() {
    m_percentage_loaded = 0;
}

symbol_matrix_t *OrderParser::parse(const std::string &path, symbol_matrix_t *matrix) {
    return matrix;
}

float OrderParser::get_percent_loaded() const {
    return m_percentage_loaded;
}

bool OrderParser::is_finished() const {
    return m_is_finished;
}

bool OrderParser::is_cancelled() const {
    return m_is_canceled;
}

void OrderParser::set_canceled() {
    m_is_canceled = true;
}

void OrderParser::finish() {
    m_is_finished = true;
}
