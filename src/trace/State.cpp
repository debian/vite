/**
 *
 * @file src/trace/State.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */

#include <string>
#include <map>
#include <list>
#include <stack>
#include <vector>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
/* -- */
using namespace std;

State::State(Date start, Date end, StateType *type, Container *container, EntityValue *value, map<std::string, Value *> opt) :
    Entity(container, std::move(opt)), _start(std::move(start)), _end(std::move(end)), _type(type), _value(value) {
}

State::State() :
    Entity(), _start(Date()), _end(Date()), _type(nullptr), _value(nullptr) { }

Date State::get_start_time() const {
    return _start;
}

Date State::get_end_time() const {
    return _end;
}

double State::get_duration() const {
    return _end - _start;
}

const StateType *State::get_type() const {
    return _type;
}

/*const EntityValue *State::get_value() const {
    return _value;
 }*/

EntityValue *State::get_value() const {
    return _value;
}
