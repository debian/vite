/**
 *
 * @file src/render/vbo/Render_alternate.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Nolan Bredel
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 * @author Francois Trahay
 * @author Thibault Soucarre
 * @author Lucas Guedon
 * @author Augustin Gauchet
 * @author Luca Bourroux
 *
 * @date 2024-07-17
 */
/*!
 *\file Render_alternate.hpp
 */

#ifndef RENDER_ALTERNATE_HPP
#define RENDER_ALTERNATE_HPP

#include <stack>
#include <cmath>
#include <deque>
#include <sstream>
/* -- */
#include <QObject>
#include <QGLWidget>
#include <QLabel>
#include <QPainter>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
// #include <ft2build.h>
// #include FT_FREETYPE_H
/* -- */
#include "common/common.hpp"
#include "common/Info.hpp"
/* -- */
#include "render/Geometry.hpp"
#include "render/Hook_event.hpp"
#include "render/Render.hpp"
#include "render/Minimap.hpp"
#include "render/Ruler.hpp"
#include "render/GanttDiagram.hpp"
#include "render/vbo/vbo.hpp"
#include "render/vbo/Shader.hpp"

#include "render/Render_windowed.hpp"

/* -- */
#include "interface/Interface_graphic.hpp"

class Render_alternate;
class Interface_console;
class StateType;

/*!
 * \def _DRAWING_CONTAINER_HEIGHT_DEFAULT
 * \brief The default height for basic containers.
 */

#define _DRAWING_CONTAINER_HEIGHT_DEFAULT 1.2f

/*!
 * \brief This class redefined the OpenGL widget - QGLWidget - to display the trace.
 */

class Render_alternate : public Hook_event
{
    Q_OBJECT
private:
    // struct for using freetype for text rendering
    // see http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Text_Rendering_02
    // struct character_info {
    //     float ax; // advance.x
    //     float ay; // advance.y
    //     float bw; // bitmap.width;
    //     float bh; // bitmap.rows;
    //     float bl; // bitmap_left;
    //     float bt; // bitmap_top;
    //     float tx; // x offset of glyph in texture coordinates
    // } char_info[128];
    // FT_Library library;
    // FT_Face face;
    // int atlas_width;
    int _glsl; // version of OpenGL Shading Language
    Shader *_shader = nullptr;
    Shader *_wait_shader = nullptr;

    // Here we are sad that we can't specify the bucket size :(.
    // We might want to implement the Bucket Array data structure ourself.
    // see >SHADER_POOL
    std::deque<Shader> _shader_pool;
    Element_pos _container_height;
    Element_col _r;
    Element_col _g;
    Element_col _b;
    std::vector<Container_text_> _texts;
    std::map<long int, Variable_text_> _variable_texts;
    glm::mat4 _modelview;
    glm::mat4 _projection;
    Vbo _containers;
    Vbo _arrows3;
    Vbo _counters;
    Vbo _ruler;
    Vbo *_wait;
    Vbo _selection;
    Vbo _time_line;
    Vbo *_current;
    GLuint _textureID = 0;
    std::map<EntityValue *, Vbo *> _states;
    // std::map<EntityValue*, Shader*> _states_shaders;
    // first element of the pair contains the points, second contains the lines
    std::map<EntityValue *, std::pair<Vbo *, Vbo *>> _events;
    // std::map<EntityValue*, Shader*> _events_shaders;
    // first element of the pair contains the line of the arrow, second contains the position of the head
    std::map<EntityValue *, std::pair<Vbo *, Vbo *>> _arrows;
    // std::map<EntityValue*, Shader*> _arrows_shaders;
    std::map<EntityValue *, std::vector<Element_pos>> _links;
    /*!
     * \brief Offset of the vertical helper line
     */
    Element_pos vertical_line;

public:
    /***********************************
     *
     * Constructor and destructor.
     *
     **********************************/

    /*!
     * \brief The default constructor
     */
    Render_alternate(Interface_graphic *interface_graphic, const QGLFormat &format);

    /*!
     * \brief The destructor
     */
    ~Render_alternate() override;

    /*!
     * \brief Set Statistics and Information about input trace
     */
    void set_total_width(Element_pos) { }

    /*!
     * \brief Set Statistics and Information about input trace
     */
    void set_total_time(Times) { }

    /*!
     * \brief display the scale
     */
    void display_time_scale() { }

    QWidget *get_render_widget() override;

    QImage grab_frame_buffer() override;

    /***********************************
     *
     * Default QGLWidget functions.
     *
     **********************************/

    /*!
     * \brief Call by the system to initialize the OpenGL render area.
     */
    void initializeGL() override;

    /*!
     * \brief Call by the system when the render area was resized (occurs during a window resizement).
     * \param width : the new width of the render area.
     * \param height : the new height of the render area.
     */
    void resizeGL(int width, int height) override;

    /*!
     * \brief Call by the system each time the render area need to be updated.
     */
    void paintGL() override;

    // void paintEvent(QPaintEvent *event);

    /* void initializeOverlayGL();
     void resizeOverlayGL(int width, int height);
     void paintOverlayGL();*/

    /***********************************
     *
     * Building functions.
     *
     **********************************/

    /*!
     * \brief This function constructs the trace.
     */
    //  bool display_build() ;

    /*!
     * \brief This function releases the trace.
     */
    //   bool display_unbuild() ;

    /*!
     * \brief Proceeds with the initialization of the OpenGL draw functions.
     */
    void start_draw() override;

    /*!
     * \brief Creates and opens the display list for container draws.
     */
    void start_draw_containers() override;

    /*!
     * \brief Draw a container according to the parameters
     * \param x the x position of the container
     * \param y the y position of the container
     * \param w the width of the container
     * \param h the height of the container
     */
    void draw_container(const Element_pos x, const Element_pos y, const Element_pos w, const Element_pos h);

    /*!
     * \brief Draw the text of a container.
     * \param x the x position of the text.
     * \param y the y position of the text.
     * \param value the string value of the text.
     *
     * This function stores text in a list. This list will be display each time the render area need to be updated.
     */
    void draw_container_text(const Element_pos x, const Element_pos y, const std::string &value);

    /*!
     * \brief Closes the container display list.
     */
    void end_draw_containers() override;

    /*!
     * \brief Creates and opens the display list for stater draws.
     */
    void start_draw_states() override;

    /*!
     * \brief Draw a state of the trace.
     * \param start the beginning time of the state.
     * \param end the ending time of the state.
     * \param base vertical position of the state.
     * \param height the state height.
     * \param r the red color rate of the state.
     * \param g the green color rate of the state.
     * \param b the blue color rate of the state.
     */
    void draw_state(const Element_pos start, const Element_pos end, const Element_pos base, const Element_pos height, const Element_pos r, EntityValue *type) override;

    /*!
     * \brief Closes the state display list.
     */
    void end_draw_states() override;

    /*!
     * \brief Open the arrow display list.
     */
    void start_draw_arrows() override;

    /*!
     * \brief Draw an arrow.
     * \param start_time the beginning time of the arrow.
     * \param end_time the ending time of the arrow.
     * \param start_height vertical position of the begining time of the arrow.
     * \param end_height vertical position of the ending time of the arrow.
     *
     * This function stores all the information of the arrow to display it each time the render area need to be updated.
     */
    void draw_arrow(const Element_pos start_time, const Element_pos end_time, const Element_pos start_height, const Element_pos end_height, const Element_col red, const Element_col green, const Element_col blue, EntityValue *value) override;

    /*!
     * \brief Closes the arrow display list.
     */
    void end_draw_arrows() override;

    /*!
     * \brief Draw arrows contained in the Arrow_ vector
     * \param arrows An arrow vector.
     */
    // void draw_stored_arrows(std::vector<Arrow_> &arrows);

    void start_draw_events() override;

    /*!
     * \brief Draw an event.
     * \param time time when the event occurs.
     * \param height vertical position of the event.
     * \param container_height information to draw event. It corresponds to the container height when they are drawn horizontally.
     *
     *
     * \brief Creates and opens the display list for container draws.
     *
     * This function stores all the information of the event to display it each time the render area need to be updated.
     */
    void draw_event(const Element_pos time, const Element_pos height, const Element_pos container_height, EntityValue *) override;

    void end_draw_events() override;

    /*!
     * \brief Draw events contained in the Event_ vector
     * \param events An event vector.
     */
    //  void draw_stored_events(std::vector<Event_> &events);

    /*!
     * \brief Creates and opens the display list for counter draws.
     */
    void start_draw_counter() override;

    /*!
     * \brief Draw a point of the counter.
     * \param x x position of the point.
     * \param y y position of the point.
     *
     * Each time counter is increased, this function is called with the coordinates of the new point.
     */
    void draw_counter(const Element_pos x, const Element_pos y);

    /*!
     * \brief Closes the counter display list.
     */
    void end_draw_counter() override;

    /*!
     * \brief Called before ruler drawing.
     */
    void start_ruler() override;

    /*!
     * \brief Called after ruler drawing.
     */
    void end_ruler() override;

    /*!
     * \brief Do nothing (it is present for compatibility of the Render class).
     */
    void end_draw() override;

    /***********************************
     *
     * Render OpenGL drawing functions.
     *
     **********************************/

    /*!
     * \brief Display a wait on the screen if there is no file opened.
     * \return Asset value of the wait.
     */
    GLuint draw_wait();

    /*!
     * \brief Draw the ruler display list.
     */
    void call_ruler() override;

    /***********************************
     *
     * Building functions.
     *
     **********************************/

    /*!
     * \brief This function draws the trace.
     */
    bool build() override;

    /*!
     * \brief This function releases the trace.
     */
    bool unbuild() override;

    /***********************************
     *
     * Displaying functions.
     *
     **********************************/

    //    /*!
    //      * \brief Display on screen containers between container_begin and container_end.
    //      * \param container_begin integer value : id of the first container.
    //      * \param container_end integer value : id of the last container.
    //      */
    //       void display_container(Element_count container_begin, Element_count container_end) =0;

    //     /*!
    //      * \brief Display on screen states between timer_begin and time_end,
    //      * container_begin and container_end and with timer width between depth_begin and depth_end.
    //      * \param time_begin floating point value : time of the first state.
    //      * \param time_end floating point value : time of the last state.
    //      * \param container_begin integer value : id of the first container.
    //      * \param container_end integer value : id of the last container.
    //      * \param depth_begin floating point value : width of the narrowest state.
    //      * \param depth_end floating point value : width of the widest state.
    //      */
    //        void display_state(Element_pos time_begin, Element_pos time_end,
    //                        Element_count container_begin, Element_count container_end,
    //                        Element_pos depth_begin, Element_pos depth_end) =0;

    //     /*!
    //      * \brief Display on screen arrows between timer_begin and time_end,
    //      * container_begin and container_end and with timer width between depth_begin and depth_end.
    //      * \param time_begin floating point value : time of the smallest arrow time value.
    //      * \param time_end floating point value : time of the higher arrow time value.
    //      * \param container_begin integer value : id of the first container.
    //      * \param container_end integer value : id of the last container.
    //      * \param depth_begin floating point value : the narrowest difference between
    //      * the beginning time and the ending time of the arrow.
    //      * \param depth_end floating point value : width of the widest difference between
    //      * the beginning time and the ending time of the arrow.
    //      */
    //     virtual void display_arrow(Element_pos time_begin, Element_pos time_end,
    //                        Element_count container_begin, Element_count container_end,
    //                        Element_pos depth_begin, Element_pos depth_end) =0;

    //     /*!
    //      * \brief Display on screen events between timer_begin and time_end,
    //      * container_begin and container_end.
    //      * \param time_begin floating point value : time of the first event.
    //      * \param time_end floating point value : time of the last event.
    //      * \param container_begin integer value : id of the first container.
    //      * \param container_end integer value : id of the last container.
    //      */
    //     virtual void display_event(Element_pos time_begin, Element_pos time_end,
    //                        Element_count container_begin, Element_count container_end) =0;

    //     /*!
    //      * \brief Display on screen counters between timer_begin and time_end,
    //      * container_begin and container_end.
    //      * \param time_begin floating point value : time of the smallest counter time value.
    //      * \param time_end floating point value : time of the higher counter time value.
    //      * \param container_begin integer value : id of the first container.
    //      * \param container_end integer value : id of the last container.
    //      */
    //     virtual void display_counter(Element_pos time_begin, Element_pos time_end,
    //                          Element_count container_begin, Element_count container_end) =0;

    /*!
     * \brief Set the color for the further drawings.
     * \param r the red value. Within [0 ; 1].
     * \param g the green value. Within [0 ; 1].
     * \param b the blue value. Within [0 ; 1].
     */
    void set_color(float r, float g, float b) override;

    /*!
     * \brief Draw a text.
     * \param x the horizontal position of the left bottom corner of the text.
     * \param y the vertical position of the left bottom corner of the text.
     * \param z the deep position of the text.
     * \param s the text.
     */
    void draw_text(const Element_pos x, const Element_pos y, const Element_pos z, const std::string &s) override;

    /*!
     * \brief Draw a quad.
     * \param x the horizontal position of the left bottom corner of the quad.
     * \param y the vertical position of the left bottom corner of the quad.
     * \param z the deep position of the quad.
     * \param w the width of the quad.
     * \param h the height of the quad.
     */
    void draw_quad(Element_pos x, Element_pos y, Element_pos z, Element_pos w, Element_pos h) override;

    /*!
     * \brief Draw a triangle.
     * \param x the horizontal position of the triangle center.
     * \param y the vertical position of the triangle center.
     * \param size the edge size.
     * \param r the rotation of triangle. (clockwise and in degree)
     */
    void draw_triangle(Element_pos x, Element_pos y,
                       Element_pos size, Element_pos r) override;

    /*!
     * \brief Draw a line.
     * \param x1 the horizontal position of the first point.
     * \param y1 the vertical position of the firt point.
     * \param x2 the horizontal position of the second point.
     * \param y2 the vertical position of the second point.
     * \param z the deep position of the triangle.
     */
    void draw_line(Element_pos x1, Element_pos y1, Element_pos x2, Element_pos y2, Element_pos z) override;

    /*!
     * \brief Draw a circle.
     * \param x the horizontal position of the circle center.
     * \param y the vertical position of the circle center.
     * \param z the deep position of the circle.
     * \param r the circle radius.
     */
    void draw_circle(Element_pos x, Element_pos y, Element_pos z, Element_pos r) override;

    /* Temporary methods. Use to draw stored arrows and circles. It is to prevent scaling */
    void draw_stored_arrows();
    void draw_stored_circles();

    void draw_stored_texts();
    void release() override;
    void draw_text_value(long int id, double text, double y) override;
    void show_minimap() override;

    /*!
     * \brief draws the vertical helper line
     */

    void draw_vertical_line() override;

    /*!
     * \brief set the vertical line offset
     * \param l the line offset.
     */
    void set_vertical_line(Element_pos l) override;

    /*!
     * \brief returns the offset of the vertical helper line
     */
    Element_pos get_vertical_line();

    void update_ev_single(EntityValue *ev,
                          std::map<EntityValue *, Vbo *> *evmap,
                          bool shaded);
    void update_ev_couple(EntityValue *ev,
                          std::map<EntityValue *, std::pair<Vbo *, Vbo *>> *evmap,
                          bool shaded);
public Q_SLOTS:
    /*!
     * \brief slot connected to the simple click event
     */
    void update_vertical_line() override;

    /*!
     * \brief Function that only delete the text of the screen
     */
    void clear_text();

    /*!
     * \brief Change the color corresponding to an EntityValue
     */

    void change_color(const std::string &entity, Element_col r, Element_col g, Element_col b);

    void change_event_color(const std::string &event, Element_col r, Element_col g, Element_col b);

    void change_link_color(const std::string &link, Element_col r, Element_col g, Element_col b);

    void change_visible(const std::string &entity, bool visible);

    void change_event_visible(const std::string &event, bool visible);

    void change_link_visible(const std::string &link, bool visible);

    /*!
     * \brief reload state color from trace file
     */
    void reload_states();

    void reload_links();

    void reload_events();

    void render_text(const QString &text, float x, float y, float w, float h);

    void update_ev(EntityValue *ev);
};

#endif
